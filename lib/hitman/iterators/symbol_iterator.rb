class SymbolIterator
  def random_string(length)
    rand(36**length).to_s(36)
  end

  def get
    [:'', random_string(10).to_sym, random_string(500).to_sym]
  end
end
